//
//  FIFATableViewCell.h
//  FinalProject
//
//  Created by blackzid on 2014/6/24.
//  Copyright (c) 2014年 blackzid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FIFATableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *pictureImageView;

@property (strong, nonatomic) IBOutlet UILabel *description;

@property (strong, nonatomic) IBOutlet UIImageView *thumbImageView;
@end

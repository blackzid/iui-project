//
//  FIFAGroupsCollectionViewController.m
//  FinalProject
//
//  Created by blackzid on 2014/6/24.
//  Copyright (c) 2014年 blackzid. All rights reserved.
//

#import "FIFAGroupsCollectionViewController.h"
#import "FIFAGroupsCollectionViewCell.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "FIFAWikiController.h"
#import "UIGestureRecognizer+BlocksKit.h"
@interface FIFAGroupsCollectionViewController ()


@end

@implementation FIFAGroupsCollectionViewController

static int numberOfGroups = 8;
static int numberOfTeamsInGroup = 4;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _groupsImages = @[@"http://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Flag_of_Brazil.svg/98px-Flag_of_Brazil.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Mexico.svg/108px-Flag_of_Mexico.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Flag_of_Croatia.svg/115px-Flag_of_Croatia.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Flag_of_Cameroon.svg/100px-Flag_of_Cameroon.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/100px-Flag_of_the_Netherlands.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Flag_of_Chile.svg/100px-Flag_of_Chile.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Spain.svg/100px-Flag_of_Spain.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/115px-Flag_of_Australia_%28converted%29.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Colombia.svg/100px-Flag_of_Colombia.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Flag_of_C%C3%B4te_d%27Ivoire.svg/100px-Flag_of_C%C3%B4te_d%27Ivoire.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Flag_of_Japan.svg/100px-Flag_of_Japan.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Greece.svg/100px-Flag_of_Greece.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Flag_of_Costa_Rica.svg/105px-Flag_of_Costa_Rica.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/100px-Flag_of_Italy.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Flag_of_Uruguay.svg/100px-Flag_of_Uruguay.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/115px-Flag_of_the_United_Kingdom.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/100px-Flag_of_France.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Flag_of_Ecuador.svg/115px-Flag_of_Ecuador.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Switzerland.svg/82px-Flag_of_Switzerland.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Flag_of_Honduras.svg/115px-Flag_of_Honduras.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Flag_of_Argentina.svg/102px-Flag_of_Argentina.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Flag_of_Nigeria.svg/115px-Flag_of_Nigeria.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Flag_of_Iran.svg/108px-Flag_of_Iran.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Flag_of_Bosnia_and_Herzegovina.svg/115px-Flag_of_Bosnia_and_Herzegovina.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/105px-Flag_of_Germany.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Flag_of_the_United_States.svg/112px-Flag_of_the_United_States.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Flag_of_Ghana.svg/100px-Flag_of_Ghana.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Portugal.svg/100px-Flag_of_Portugal.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Flag_of_Belgium.svg/88px-Flag_of_Belgium.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_Algeria.svg/100px-Flag_of_Algeria.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Russia.svg/100px-Flag_of_Russia.svg.png"
                     ,@"http://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Flag_of_South_Korea.svg/100px-Flag_of_South_Korea.svg.png"];
    _groupsNames = @[@"巴西"
                     ,@"墨西哥"
                     ,@"克羅埃西亞"
                     ,@"喀麥隆"
                     ,@"荷蘭"
                     ,@"智利"
                     ,@"西班牙"
                     ,@"澳洲"
                     ,@"哥倫比亞"
                     ,@"象牙海岸"
                     ,@"日本"
                     ,@"希臘"
                     ,@"哥斯大黎加"
                     ,@"義大利"
                     ,@"烏拉圭"
                     ,@"英格蘭"
                     ,@"法國"
                     ,@"厄瓜多"
                     ,@"瑞士"
                     ,@"宏都拉斯"
                     ,@"阿根廷"
                     ,@"奈及利亞"
                     ,@"伊朗"
                     ,@"波士尼亞與赫塞哥維納"
                     ,@"德國"
                     ,@"美國"
                     ,@"迦納"
                     ,@"葡萄牙"
                     ,@"比利時"
                     ,@"阿爾及利亞"
                     ,@"俄羅斯"
                     ,@"南韓"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UICollectionViewDataSource Methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return numberOfGroups;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return numberOfTeamsInGroup ;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"FIFAGroupsCellIdentifier";
    FIFAGroupsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.image.userInteractionEnabled = YES;
    cell.image.layer.borderColor = [[UIColor grayColor] CGColor];
    cell.image.layer.borderWidth = 2;
    cell.name.text = _groupsNames[indexPath.section*4+indexPath.item];
    [cell.image setImageWithURL:[NSURL URLWithString:_groupsImages[indexPath.section*4+indexPath.item]]];
    NSString *url = [NSString stringWithFormat:@"http://zh.wikipedia.org/wiki/%@",_groupsNames[indexPath.section*4+indexPath.item]];
    UIGestureRecognizer *recognizer =[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location){
        if (UIGestureRecognizerStateRecognized == state){
            FIFAWikiController *viewcontroller = [[FIFAWikiController alloc] initWithUrl:url];
            [self.navigationController pushViewController:viewcontroller animated:YES];
        }
    }];
    [cell.image addGestureRecognizer:recognizer];
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  FIFATableViewController.h
//  FinalProject
//
//  Created by blackzid on 2014/6/24.
//  Copyright (c) 2014年 blackzid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FIFATableViewController : UITableViewController

@property (strong,nonatomic) NSMutableArray *imageUrl;
@property (strong,nonatomic) NSMutableArray *descriptions;
@end

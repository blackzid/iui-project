//
//  FIFATableViewController.m
//  FinalProject
//
//  Created by blackzid on 2014/6/24.
//  Copyright (c) 2014年 blackzid. All rights reserved.
//

#import "FIFATableViewController.h"
#import "FIFATableViewCell.h"
#import <FacebookSDK/FacebookSDK.h>
@interface FIFATableViewController ()

@end

@implementation FIFATableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self openSessionWithAllowLoginUI:YES];
    [FBRequestConnection startWithGraphPath:@"/592369040883924/feed?fields=from,picture,application"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              NSLog(@"start request")   ;
                              if(!error){
                                  for(NSDictionary *dictionary in [result objectForKey:@"data"]){
                                      if(![[[dictionary objectForKey:@"from"] objectForKey:@"id"] isEqualToString:@"592369040883924"]
                                         && [dictionary objectForKey:@"picture"]!=NULL){
                                          [_imageUrl addObject:[NSURL URLWithString:[dictionary objectForKey:@"picture"]]];
                                          [_descriptions addObject:[NSURL URLWithString:[dictionary objectForKey:@"description"]]];
                                      }
                                  }
                                  
                              }
                              else {
                                  NSLog(@"error :%@",error);
                              }
    
                          }];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"publish_actions",
                            @"public_profile",
                            @"read_stream",
                            @"user_photos",
                            @"email",
                            //@"publish_stream",
                            @"user_likes",
                            nil];
    return [FBSession openActiveSessionWithReadPermissions:permissions
                                              allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                                  switch (status) {
                                                      case FBSessionStateOpen:
                                                          break;
                                                      case FBSessionStateClosed:
                                                          break;
                                                      case FBSessionStateCreated:
                                                          break;
                                                      case FBSessionStateCreatedOpening:
                                                          break;
                                                      case FBSessionStateClosedLoginFailed:
                                                          break;
                                                      case FBSessionStateOpenTokenExtended:
                                                          break;
                                                      case FBSessionStateCreatedTokenLoaded:
                                                          break;
                                                  }
                                                  
                                              }];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}
-(void)RequestForData{
//    [FBRequestConnection startWithGraphPath:@"509903539073685/feed?fields=from,picture,application" completionHandler:^(FBRequestConnection *connection, id<FBGraphUser> result, NSError *error) {
//        if(error){
//        
//        }
//        else {
//            
//        }
//    
//    
//    }];
    NSLog(@"request data");
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"FBcellidentifier";
    FIFATableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

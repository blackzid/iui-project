//
//  FIFAWikiController.h
//  FinalProject
//
//  Created by blackzid on 2014/6/24.
//  Copyright (c) 2014年 blackzid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FIFAWikiController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong,nonatomic ) NSString* url;
- (id)initWithUrl:(NSString *) url;
@end

//
//  FIFAGroupsCollectionViewController.h
//  FinalProject
//
//  Created by blackzid on 2014/6/24.
//  Copyright (c) 2014年 blackzid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FIFAGroupsCollectionViewController : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate,UIGestureRecognizerDelegate>


@property (strong,nonatomic) NSArray *groupsImages;
@property (strong,nonatomic) NSArray *groupsNames;
@property (strong,nonatomic) NSArray *groupsurl;
@end
